$(document).ready(function() {
    // featuredGames tab --- ((
    $('.featuredGames__tab li:first-child').addClass('active');
    $('.tab-item').hide();
    $('.tab-item:first').show();

    $('.featuredGames__tab li').click(function(){
    $('.featuredGames__tab li').removeClass('active');
    $(this).addClass('active');
    $('.tab-item').hide();
    
    var activeTab = $(this).find('a').attr('href');
    $(activeTab).fadeIn();
    return false;
    });
    // featuredGames tab --- ))

    // mobile menu ((
    // open 
    $('.mobileMenu-button').click(function() {
        $('.mobileMenu').css('display', 'block')
        $('.mobileMenu__item').removeClass("close-menu")
        $('.mobileMenu__item').addClass("open-menu")
        $('body').css('overflow', 'hidden')
    })

    // close
    let mobileMenu = $('.mobileMenu').get(0)
    window.onclick = function(e) {
        if(e.target == mobileMenu) {
            $('.mobileMenu__item').addClass("close-menu")
            setTimeout(function() {
                $('body').css('overflow', 'auto')
                $('.mobileMenu').css('display', 'none')
                $('.mobileMenu__item').removeClass("open-menu")
            }, 400)
        }
    }
    // mobile menu ))
})
